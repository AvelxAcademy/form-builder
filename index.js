import FormBuilder from "./organisms/Form.vue";

export default {
  install(Vue, options) {
    Vue.component('FormBuilder', FormBuilder)
  }
}